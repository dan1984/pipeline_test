package config

import (
	"flag"
	"fmt"
	"os"

	"github.com/joho/godotenv"
)

// Config - holds env related data
type Config struct {
	Port string
	Env  string
	DSN  string
}

// Get loads .env file and uses it as defualt values for our env variables
// flag is used to get some documentation out of the box when runnig app with -help flag
func Get() *Config {
	if err := godotenv.Load(); err != nil {
		fmt.Println("Cant load the .env file but vars might be still accessible", err)
	}

	conf := &Config{}

	flag.StringVar(&conf.Port, "port", os.Getenv("PORT"), "HTTP network port")
	flag.StringVar(&conf.DSN, "dbConnStr", os.Getenv("DSN"), "DB data source name")
	flag.StringVar(&conf.Env, "env", os.Getenv("ENV"), "Environment name: dev, staging, prod")
	flag.Parse()

	return conf
}
