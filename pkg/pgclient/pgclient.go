package pgclient

import (
	"database/sql"
)

// Get - returns pg client and/or error
func Get(connStr string) (*sql.DB, error) {
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		return nil, err
	}

	// for now, for simplicity, keeping it as default
	// db.SetConnMaxLifetime(0)
	// db.SetMaxIdleConns(3)
	// db.SetMaxOpenConns(3)

	return db, nil
}
