package main

import (
	"fmt"

	"bitbucket.org/pipeline_test/cmd/samplepackage"
)

func main() {
	fmt.Println("HOLA from server")
	jobDone := samplepackage.Work("wash dishes")
	fmt.Println("=====> ", jobDone)
}
