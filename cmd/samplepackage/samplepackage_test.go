package samplepackage_test

import (
	"fmt"
	"os"
	"testing"

	"bitbucket.org/pipeline_test/cmd/samplepackage"
	"bitbucket.org/pipeline_test/pkg/config"
	"bitbucket.org/pipeline_test/pkg/pgclient"
	_ "github.com/lib/pq"
)

func TestWork(t *testing.T) {
	fmt.Println("+++++------+++++------->>> ", os.Getenv("DOCKER_HUB_USERNAME"))
	cfg := config.Get()
	t.Log("---------------------------", cfg.DSN)
	db, err := pgclient.Get(cfg.DSN)
	if err != nil {
		t.Errorf("### Failed initiating DB %s", err)
	}
	expected := "I did the job:wash dishes"
	got := samplepackage.Work("wash dishes")
	t.Log("ALL GOOOOOD ++++++++++++++++++++++++++++++")
	if expected != got {
		t.Errorf("Expected:%s; Got:%s", expected, got)
	}

	db.Close()
}
